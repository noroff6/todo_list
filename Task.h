#ifndef TASK_H
#define TASK_H

#include <QWidget>
#include <QString>
#include <QDeadlineTimer>
#include <QTimer>
#include <QFontDialog>

namespace Ui {
class Task;
}

class Task : public QWidget
{
    Q_OBJECT

public:
    explicit Task(const QString& name, const QString& description, const QString& assigned, QWidget *parent = 0);
    ~Task();

    void setName(const QString& name, const QString& description, const QString& assigned);
    QString name() const;
    QString description() const;
    QString assigned() const;
    bool isCompleted() const;

public slots:
    void rename();

signals:
    void removed(Task* task);
    void statusChanged(Task* task);

private slots:
    void checked(bool checked);


    void on_dateTimeEdit_dateTimeChanged(const QDateTime &dateTime);

private:
    Ui::Task *ui;
};

#endif // TASK_H
