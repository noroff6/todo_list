#include "Task.h"
#include "ui_Task.h"

#include <QInputDialog>
#include <QDebug>

Task::Task(const QString& name, const QString& description, const QString& assign, QWidget *parent) :
        QWidget(parent),
        ui(new Ui::Task)
{
    ui->setupUi(this);
    setName(name, description, assign);

    connect(ui->editButton, &QPushButton::clicked, this, &Task::rename);
    connect(ui->removeButton, &QPushButton::clicked, [this] {
        emit removed(this);
    });
    connect(ui->checkbox, &QCheckBox::toggled, this, &Task::checked);


}

Task::~Task()
{
    qDebug() << "~Task() called";
    delete ui;
}

void Task::setName(const QString& name, const QString& description, const QString& assigned)
{
    ui->checkbox->setText(name.toUpper());
    ui->label->setText("[" + description + "]");
    ui->dateTimeEdit->setDateTime(QDateTime::currentDateTime());
    ui->assigned_to->setText(assigned);
}




QString Task::name() const
{
   return ui->checkbox->text();
}

QString Task::description() const
{
    return ui->label->text();

}

QString Task::assigned() const
{
    return ui->assigned_to->text();

}

bool Task::isCompleted() const
{
   return ui->checkbox->isChecked();
}

void Task::rename()
{
    bool ok;
    QString name_value = QInputDialog::getText(this, tr("Edit task"),
                                          tr("Task name"), QLineEdit::Normal,
                                          this->name(), &ok);

    QString description_value = QInputDialog::getText(this, tr("Edit description"),
                                          tr("Task description"), QLineEdit::Normal,
                                          this->description(), &ok);

    QString assign_value = QInputDialog::getText(this, tr("Edit description"),
                                          tr("Task description"), QLineEdit::Normal,
                                          this->assigned(), &ok);



    if (ok && !name_value.isEmpty() && !description_value.isEmpty()) {
        setName(name_value, description_value, assign_value);
    }
}

void Task::checked(bool checked)
{
    QFont font(ui->checkbox->font());
    font.setStrikeOut(checked);
    ui->checkbox->setFont(font);

    emit statusChanged(this);
}




void Task::on_dateTimeEdit_dateTimeChanged(const QDateTime &dateTime)
{
    ui->dateTimeEdit->calendarPopup();
    QString start = QDateTime().currentDateTime().toString("hh:mm:ss AP dd/MM/yyyy");
    QString end =dateTime.toString("hh:mm:ss AP dd/MM/yyyy");
    QString timeDiff= QString("%1").arg(QDateTime().fromString(start ,"hh:mm:ss AP dd/MM/yyyy").msecsTo(QDateTime().fromString(end ,"hh:mm:ss AP dd/MM/yyyy")));
    int diff = timeDiff.toInt();
    QDeadlineTimer deadline(diff);

    if(deadline.hasExpired())
    {
        ui->dateTimeEdit->setStyleSheet("background-color: red;");
    }
    else
    {
        ui->dateTimeEdit->setStyleSheet("background-color: green;");
    }
}

