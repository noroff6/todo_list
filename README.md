# TO DO application with C++ and Qt
Based on this repo: [qt_todo_list](https://gitlab.com/plczapiewski/qt_todo_list)

# Menu
![](todo_menu.png)


# Adding to-do:s
![](tasks.PNG)


# Qt
[Download Qt](https://www.qt.io/download)
