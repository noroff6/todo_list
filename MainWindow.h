#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVector>

#include "Task.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void updateStatus();

public slots:
    void addTask();
    void removeTask(Task* task);
    void taskStatusChanged(Task* task);

private slots:
    void on_actionChange_font_triggered();
    void on_actionRed_triggered();
    void on_actionBlue_triggered();
    void on_actionPink_triggered();
    void on_actionWhite_triggered();

private:
    Ui::MainWindow *ui;
    QVector<Task*> mTasks;
    QPalette *sample_palette = nullptr;
};

#endif // MAINWINDOW_H
