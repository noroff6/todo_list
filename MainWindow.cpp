#include "MainWindow.h"
#include "ui_MainWindow.h"

#include <QDebug>
#include <QInputDialog>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    mTasks()
{
    ui->setupUi(this);
    connect(ui->addTaskButton, &QPushButton::clicked, this, &MainWindow::addTask);
    updateStatus();
    sample_palette = new QPalette();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::addTask()
{
    bool ok;
    QString name = QInputDialog::getText(this, tr("Add task"),
                                         tr("Task name"), QLineEdit::Normal,
                                         tr("Untitled task"), &ok);

    QString description = QInputDialog::getText(this, tr("Add description"),
                                         tr("Description"), QLineEdit::Normal,
                                         tr("Untitled description"), &ok);

    QString assigned = QInputDialog::getText(this, tr("Assign to"),
                                         tr("Name"), QLineEdit::Normal,
                                         tr("Untitled description"), &ok);

    if (ok && !name.isEmpty() && !description.isEmpty()) {
        qDebug() << "Adding new task";
        Task* task = new Task(name, description, assigned);
        connect(task, &Task::removed, this, &MainWindow::removeTask);
        connect(task, &Task::statusChanged, this, &MainWindow::taskStatusChanged);
        mTasks.append(task);
        ui->tasksLayout->addWidget(task);
        updateStatus();
    }
}

void MainWindow::removeTask(Task* task)
{
    mTasks.removeOne(task);
    ui->tasksLayout->removeWidget(task);
    delete task;
    updateStatus();
}

void MainWindow::taskStatusChanged(Task* /*task*/)
{
    updateStatus();
}

void MainWindow::updateStatus()
{
    int completedCount = 0;
    for(auto t : mTasks)  {
        if (t->isCompleted()) {
            completedCount++;
        }
    }
    int todoCount = mTasks.size() - completedCount;

    ui->statusLabel->setText(QString("Status: %1 todo / %2 completed")
                             .arg(todoCount)
                             .arg(completedCount));
}

void MainWindow::on_actionChange_font_triggered()
{
    qDebug() << "In function";
    MainWindow::setFont(QFontDialog::getFont(0, MainWindow::font()));
}


void MainWindow::on_actionRed_triggered()
{
    sample_palette->setColor(QPalette::Window, Qt::red);
    sample_palette->setColor(QPalette::WindowText, Qt::white);
    MainWindow::setAutoFillBackground(true);
    MainWindow::setPalette(*sample_palette);

}


void MainWindow::on_actionBlue_triggered()
{
    sample_palette->setColor(QPalette::Window, Qt::blue);
    sample_palette->setColor(QPalette::WindowText, Qt::white);
    MainWindow::setAutoFillBackground(true);
    MainWindow::setPalette(*sample_palette);
}


void MainWindow::on_actionPink_triggered()
{
    sample_palette->setColor(QPalette::Window, Qt::darkMagenta);
    sample_palette->setColor(QPalette::WindowText, Qt::white);
    MainWindow::setAutoFillBackground(true);
    MainWindow::setPalette(*sample_palette);
}


void MainWindow::on_actionWhite_triggered()
{
    sample_palette->setColor(QPalette::Window, Qt::white);
    sample_palette->setColor(QPalette::WindowText, Qt::black);
    MainWindow::setAutoFillBackground(true);
    MainWindow::setPalette(*sample_palette);
}

